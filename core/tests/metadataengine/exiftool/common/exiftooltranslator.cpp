/* ============================================================
 *
 * This file is a part of digiKam project
 * https://www.digikam.org
 *
 * Date        : 2021-04-13
 * Description : ExifTool tags translator for Exiv2
 *
 * Copyright (C) 2020-2021 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "exiftooltranslator_p.h"

// Qt includes

#include <QHash>
#include <QStringList>

namespace Digikam
{

ExifToolTranslator::ExifToolTranslator()
    : d(new Private)
{
}

ExifToolTranslator::~ExifToolTranslator()
{
    delete d;
}

bool ExifToolTranslator::isIgnoredGroup(const QString& group) const
{
    foreach (const QString& bgrp, d->ignoreGroupsET)
    {
        if (group.startsWith(bgrp))
        {
            return true;
        }
    }

    return false;
}

QString ExifToolTranslator::translateToExiv2(const QString& tagName) const
{
    QHash<QString, QString>::iterator it = d->mapETtoExiv2.find(tagName);

    if (it != d->mapETtoExiv2.end())
    {
        return it.value();
    }

    return QString();
}

QString ExifToolTranslator::translateToExifTool(const QString& tagName) const
{
    QHash<QString, QString>::iterator it = d->mapExiv2toET.find(tagName);

    if (it != d->mapExiv2toET.end())
    {
        return it.value();
    }

    return QString();
}

} // namespace Digikam
